SUMMARY
-------

"Context Reaction: Front page" provides a reaction for the Context module which
lets you to change the default front page.

REQUIREMENTS
------------

Module Context 3: https://www.drupal.org/project/context

INSTALLATION
------------

Install as usual, see http://drupal.org/node/70151 for further information.

USE CASE
--------

I have a client who wants to customize the front page by user role to display a
dedicated branding page to the users.
The webmaster creates many node of a specific content type than necessary and it
displays the correct node with a context that has one condition "User role" and
one reaction "Front page" to change the default front page.

FAQ
---

Q: Is this module compatible with the module Domain Configuration?
A: Yes.

Q: Why do not having contributed to the module Context Plugin Extras which
   provides an equivalent module for Drupal 6?
A: I wanted to give this module to the community without having to migrate all
   the Context Plugin Extra modules to Drupal 7.

CONTACT
-------

Current maintainer:

* Mehdi Kabab (piouPiouM) – http://drupal.org/user/2110250

